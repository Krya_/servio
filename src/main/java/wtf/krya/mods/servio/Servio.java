package wtf.krya.mods.servio;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Servio implements ModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("servio");

    @Override
    public void onInitialize() {
        LOGGER.info("Initializing");
    }
}

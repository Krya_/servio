package wtf.krya.mods.servio.mixin;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.server.command.PublishCommand;
import net.minecraft.server.command.ServerCommandSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PublishCommand.class)
public class PublishCommandMixin {
    /**
     * @author Krya
     * @reason Unregistering '/register' command
     */
    @Overwrite
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {}
}

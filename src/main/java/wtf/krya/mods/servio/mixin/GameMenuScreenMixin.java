package wtf.krya.mods.servio.mixin;

import net.minecraft.client.gui.screen.GameMenuScreen;
import net.minecraft.client.gui.screen.OpenToLanScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.Objects;

@Mixin(GameMenuScreen.class)
public class GameMenuScreenMixin extends Screen {
    protected GameMenuScreenMixin(Text title) {
        super(title);
    }

    private static final Text OPEN_TO_FRIENDS_TEXT = Text.translatable("menu.shareOnline");
    private static final Text ONLINE_CONFIG = Text.translatable("menu.onlineConfig");

    @Inject(method = "initWidgets", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/MinecraftClient;isIntegratedServerRunning()Z"), locals = LocalCapture.CAPTURE_FAILHARD)
    public void LanServerProperties(CallbackInfo ci, int i, int j, String string, ButtonWidget buttonWidget, ButtonWidget oldButton){
        oldButton.visible = false;
        ButtonWidget shareWithFriendsButton = new ButtonWidget(width / 2 + 4, height / 4 + 96 + 16, 98, 20, OPEN_TO_FRIENDS_TEXT,
                (button) -> Objects.requireNonNull(client).setScreen(new OpenToLanScreen(this))
        );
        addDrawableChild(shareWithFriendsButton);

        if (client != null && client.isIntegratedServerRunning() && client.getServer().isRemote()) {
            shareWithFriendsButton.setMessage(ONLINE_CONFIG);
        }
        shareWithFriendsButton.active = client.isIntegratedServerRunning();
    }
}
